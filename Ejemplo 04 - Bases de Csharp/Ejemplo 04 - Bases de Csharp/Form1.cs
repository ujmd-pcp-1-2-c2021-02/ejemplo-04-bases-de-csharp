﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_04___Bases_de_Csharp
{
    public partial class Form1 : Form
    {
        double Operando1 = 0;
        double Operando2 = 0;
        double Resultado = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Este es un comentario de una misma línea
            int a = 1;    // Este es un comentario seguido a un código

            /*  Este es un
             *  comentario
             *  de multiples
             *  líneas
             */
            
        }

        // Este es otro tipo de comentario especial para funciones
        // Se crea su estructura escribiendo "/" tres veces.
        /// <summary>
        /// Esta función hace la suma de dos números
        /// </summary>
        /// <param name="num1">Sumando 1</param>
        /// <param name="num2">Sumando 2</param>
        /// <returns>El resultado de la suma</returns>
        private int sumar(int num1, int num2)
        {
            return num1 + num2;
        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            Resultado = Operando1 + Operando2;

            txtResultado.Text = Resultado.ToString();
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            Resultado = Operando1 - Operando2;

            txtResultado.Text = Resultado.ToString();
        }

        private void btnProducto_Click(object sender, EventArgs e)
        {
            Resultado = Operando1 * Operando2;

            txtResultado.Text = Resultado.ToString();
        }

        private void btnCociente_Click(object sender, EventArgs e)
        {
            Resultado = Operando1 / Operando2;

            txtResultado.Text = Resultado.ToString();
        }

        private void txtOperando1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Operando1 = Convert.ToDouble(txtOperando1.Text);
            }
            catch (Exception)
            {
                Operando1 = 0;
                txtOperando1.Text = "0";
            }
        }

        private void txtOperando2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Operando2 = Convert.ToDouble(txtOperando2.Text);
            }
            catch (Exception)
            {
                Operando2 = 0;
                txtOperando2.Text = "0";
            }
        }

        private void btnResiduo_Click(object sender, EventArgs e)
        {
            Resultado = Operando1 % Operando2;

            txtResultado.Text = Resultado.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Resultado = Math.Pow(Operando1, Operando2);

            txtResultado.Text = Resultado.ToString();
        }
    }
}
