﻿
namespace Ejemplo_04___Bases_de_Csharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOperando1 = new System.Windows.Forms.TextBox();
            this.txtOperando2 = new System.Windows.Forms.TextBox();
            this.btnSuma = new System.Windows.Forms.Button();
            this.btnResta = new System.Windows.Forms.Button();
            this.btnProducto = new System.Windows.Forms.Button();
            this.btnCociente = new System.Windows.Forms.Button();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.btnResiduo = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOperando1
            // 
            this.txtOperando1.Font = new System.Drawing.Font("Consolas", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOperando1.Location = new System.Drawing.Point(48, 12);
            this.txtOperando1.Name = "txtOperando1";
            this.txtOperando1.Size = new System.Drawing.Size(185, 64);
            this.txtOperando1.TabIndex = 0;
            this.txtOperando1.Text = "0";
            this.txtOperando1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOperando1.TextChanged += new System.EventHandler(this.txtOperando1_TextChanged);
            // 
            // txtOperando2
            // 
            this.txtOperando2.Font = new System.Drawing.Font("Consolas", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOperando2.Location = new System.Drawing.Point(343, 12);
            this.txtOperando2.Name = "txtOperando2";
            this.txtOperando2.Size = new System.Drawing.Size(185, 64);
            this.txtOperando2.TabIndex = 1;
            this.txtOperando2.Text = "0";
            this.txtOperando2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOperando2.TextChanged += new System.EventHandler(this.txtOperando2_TextChanged);
            // 
            // btnSuma
            // 
            this.btnSuma.Location = new System.Drawing.Point(258, 12);
            this.btnSuma.Name = "btnSuma";
            this.btnSuma.Size = new System.Drawing.Size(60, 22);
            this.btnSuma.TabIndex = 2;
            this.btnSuma.Text = "+";
            this.btnSuma.UseVisualStyleBackColor = true;
            this.btnSuma.Click += new System.EventHandler(this.btnSuma_Click);
            // 
            // btnResta
            // 
            this.btnResta.Location = new System.Drawing.Point(258, 40);
            this.btnResta.Name = "btnResta";
            this.btnResta.Size = new System.Drawing.Size(60, 22);
            this.btnResta.TabIndex = 2;
            this.btnResta.Text = "-";
            this.btnResta.UseVisualStyleBackColor = true;
            this.btnResta.Click += new System.EventHandler(this.btnResta_Click);
            // 
            // btnProducto
            // 
            this.btnProducto.Location = new System.Drawing.Point(258, 68);
            this.btnProducto.Name = "btnProducto";
            this.btnProducto.Size = new System.Drawing.Size(60, 22);
            this.btnProducto.TabIndex = 2;
            this.btnProducto.Text = "×";
            this.btnProducto.UseVisualStyleBackColor = true;
            this.btnProducto.Click += new System.EventHandler(this.btnProducto_Click);
            // 
            // btnCociente
            // 
            this.btnCociente.Location = new System.Drawing.Point(258, 96);
            this.btnCociente.Name = "btnCociente";
            this.btnCociente.Size = new System.Drawing.Size(60, 22);
            this.btnCociente.TabIndex = 2;
            this.btnCociente.Text = "÷";
            this.btnCociente.UseVisualStyleBackColor = true;
            this.btnCociente.Click += new System.EventHandler(this.btnCociente_Click);
            // 
            // txtResultado
            // 
            this.txtResultado.Font = new System.Drawing.Font("Consolas", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultado.Location = new System.Drawing.Point(48, 180);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.ReadOnly = true;
            this.txtResultado.Size = new System.Drawing.Size(480, 64);
            this.txtResultado.TabIndex = 1;
            this.txtResultado.Text = "0";
            this.txtResultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnResiduo
            // 
            this.btnResiduo.Location = new System.Drawing.Point(258, 124);
            this.btnResiduo.Name = "btnResiduo";
            this.btnResiduo.Size = new System.Drawing.Size(60, 22);
            this.btnResiduo.TabIndex = 2;
            this.btnResiduo.Text = "%";
            this.btnResiduo.UseVisualStyleBackColor = true;
            this.btnResiduo.Click += new System.EventHandler(this.btnResiduo_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(258, 152);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 22);
            this.button1.TabIndex = 2;
            this.button1.Text = "^";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 291);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnResiduo);
            this.Controls.Add(this.btnCociente);
            this.Controls.Add(this.btnProducto);
            this.Controls.Add(this.btnResta);
            this.Controls.Add(this.btnSuma);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.txtOperando2);
            this.Controls.Add(this.txtOperando1);
            this.Name = "Form1";
            this.Text = "Calculadora";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOperando1;
        private System.Windows.Forms.TextBox txtOperando2;
        private System.Windows.Forms.Button btnSuma;
        private System.Windows.Forms.Button btnResta;
        private System.Windows.Forms.Button btnProducto;
        private System.Windows.Forms.Button btnCociente;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.Button btnResiduo;
        private System.Windows.Forms.Button button1;
    }
}

